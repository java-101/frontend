# Frontend
Structure:
* global
  * models
  * services
    * base service(centralized service)
* page
  * Notes
  * Notebooks
  * Login
  * Register
* public
  * main module / main component
* templates
  * header
  * footer
  * theme
  * body
  
Note:
* all html, scss, ts are in the same page/component folder
* you might want to add use proxy config and add it on pckage.json start script in case your using another host or just allow your frontend/client host on backend/java crossorigins.


TODO:
* Notes
* Authentication
* Security

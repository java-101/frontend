import { Injectable } from "@angular/core";
import { Http } from "@angular/http";
import { BaseService } from "../base.service";
import {SocketService} from "../websocket/socket";
@Injectable()
export class RequestsService extends BaseService{

    constructor( private h: Http, private socket: SocketService){
        super( h );
    }

    saveItem( path: string= "", data, successCallback,  errorCallback )  {
        this.httpost( path, data, res =>{
          successCallback( res );
          // this.socket.socketCreate( res );
        }, error =>errorCallback( error ) );
    }

    getList( path: string = "", successCallback, errorCallback ){
      this.httpget( path, res =>{
        successCallback( res );
      }, error => errorCallback ( error ) );
    }

    getItemById( path: string = "", data , sucessCallback, errorCallback){
      this.httpost( path, data, res =>{
        sucessCallback( res );
      }, error=> errorCallback( error ) )
    }

    removeItem( path: string = "", data, successCallback, errorCallback ){
      this.httpost( path, data, res =>{
        successCallback( res );
      }, err=>{
        errorCallback( err );
        console.log('err', err)
      })
    }

}

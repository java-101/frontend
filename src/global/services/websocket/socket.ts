import { Injectable } from "@angular/core";
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import * as io from 'socket.io-client';
@Injectable()
export class SocketService{
  private url = 'http://localhost:5000';
  private socket;

  socketDelete( id){
    this.socket.emit( "delete", id );
  }
  socketCreate( stringified ){
    this.socket.emit( "new", stringified );
  }

  socketEdit( data ){
    this.socket.emit( "edit", data );
  }

  getCreated(){
    let observable = new Observable( observer =>{
      this.socket = io( this.url );
      this.socket.on( 'created', ( data ) =>{
        observer.next( data );
      });
      return ()=>{
        this.socket.disconnect();
      }
    });
    return observable;
  }
  getDeleted() {
    let observable = new Observable(observer => {
      this.socket = io(this.url);
      this.socket.on('deleted', (data) => {
        observer.next(data);
      });
      return () => {
        this.socket.disconnect();
      };
    });
    return observable;
  }
}

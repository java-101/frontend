

class listDTO{
  public title: string = "";
  public userId: number = null;
  public description: string = "";
  constructor(){}
}

export class editListDTO extends listDTO{
  public notebookId: number;
  constructor(){
    super();
  }
}

export class createListDTO extends listDTO{
  constructor(){
    super();
  }
}

export interface listInterface{
  notebookId?: number;
  title: string;
  userId: string;
  description: string;
}

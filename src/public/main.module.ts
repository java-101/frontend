import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './main.component';
import { PageModule, ROUTES } from "../page/page.module";
import {RouterModule} from "@angular/router";
import { TemplateModule } from "../template/tempalate.module";
import {ModalModule} from "ngx-bootstrap/modal";
import {SocketService} from "../global/services/websocket/socket";
@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    PageModule,
    TemplateModule,
    RouterModule.forRoot( ROUTES ),
    ModalModule.forRoot(),
  ],
  providers: [ SocketService ],
  bootstrap: [AppComponent]
})
export class AppModule { }

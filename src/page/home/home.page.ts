import {Component, OnInit, TemplateRef} from "@angular/core";
import {RequestsService} from "../../global/services/requests-service/requests.service";
import { listInterface } from "../../global/models/list.model";
import {BsModalRef, BsModalService} from "ngx-bootstrap";
import {ActivatedRoute, Router} from "@angular/router";
import {SocketService} from "../../global/services/websocket/socket";
// import {SocketService} from "../../global/services/websocket/socket";
@Component({
  moduleId: module.id,
  templateUrl: 'home.page.html',
  styleUrls: [ 'home.page.scss' ],
  providers: [ BsModalService ]
})

export class HomePage implements OnInit{
  srv;
  srv2;
  list = [];
  public modalRef: BsModalRef;
  constructor(
    private reqService: RequestsService,
    private modalService: BsModalService,
    private activeRoute: ActivatedRoute,
    private router: Router,
    private socket: SocketService
    // private socket: SocketService
  ){}
  ngOnInit(){

    this.reqService.getList( "lists", res=>{
      console.log( res );
    }, err=>{})

    this.srv = this.socket.getDeleted().subscribe( res =>{
      this.list.splice(res['id'], 1);
    });

    this.srv2 = this.socket.getCreated().subscribe( res =>{
      if( res['mode'] ==  "add")this.list.push( res['obj'] );
      else if( res['mode'] == "edit") {
        this.list.filter( item =>{
          if( item.id == res['obj'].id ) {
            item.name = res['obj'].name;
            item.price = res['obj'].price;
            item.description = res['obj'].description;
          }
        })
      }
    })
    this.reqService.getList( 'lists', res=>{
      console.log('see', this.list);
      this.list = res;
      console.log( res );
    }, error=>{
      alert( error );
    })
    this.activeRoute.url.subscribe(res => console.log( res ));
  }
  onClickDelete( id, idx ){
    let alrt = confirm(" Are you sure you want to delete this?");
    if( alrt ){
      this.reqService.removeItem( "delete", id, res=>{
        // this.list.splice( idx, 1);
        this.socket.socketDelete( idx );
        console.log(res);
      }, error => alert("error") )
    }
  }

  onClickSelect( item ){
    this.router.navigate([`edit/${item.id}`])
  }


}

import { NgModule } from "@angular/core";
import {HomePage} from "./home/home.page";
import {RequestsService} from "../global/services/requests-service/requests.service";
import { Routes } from "@angular/router";
import {FormsModule} from "@angular/forms";
import {BrowserModule} from "@angular/platform-browser";
import {CreatePage} from "./create/create.page";
import { ToastrModule } from 'ngx-toastr';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";

export const ROUTES: Routes = [
  { path: 'home', component: HomePage },
  { path: '', redirectTo: 'home', pathMatch: 'full'},
  { path: 'create', component: CreatePage },
  { path: 'edit/:id', component: CreatePage }
]
@NgModule({
  imports: [
    FormsModule,
    BrowserModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot()
  ],
  declarations: [
    HomePage,
    CreatePage
  ],
  exports: [],
  providers: [ RequestsService ]
})
export class PageModule{}

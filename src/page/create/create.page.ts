import {Component, OnInit, ViewContainerRef} from "@angular/core";
import {RequestsService} from "../../global/services/requests-service/requests.service";
import {createListDTO} from "../../global/models/list.model";
import {ActivatedRoute, Router} from "@angular/router";
import {SocketService} from "../../global/services/websocket/socket";
import { ToastrService } from 'ngx-toastr';
@Component({
  moduleId: module.id,
  selector: 'create',
  templateUrl: 'create.page.html',
  styleUrls: [ 'create.page.scss' ],
  providers: [ RequestsService ]
})
export class CreatePage implements OnInit{
  itemForm: createListDTO = new createListDTO();
  itmForm2: createListDTO = new createListDTO();
  isEdit: boolean = false;
  srv: any;
  constructor(
    private reqService: RequestsService,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private socket: SocketService,
    private toastr: ToastrService
  ){  }
  ngOnInit(){
    this.isEdit = false;
    this.srv = this.socket.getCreated().subscribe( res =>{
      console.log('res',res);
      setTimeout( () =>{
        this.toastr.success(`${res['obj'].name} is created`, 'Success!');
      }, 1000)

    });
    this.activeRoute.params.subscribe(res => {
      if( res['id']){
        console.log( res['id']);
        this.reqService.getItemById( 'getbyid', res['id'], s=>{
          console.log('response', s);
          this.isEdit = true;
          this.itemForm = s;
          this.itmForm2 = JSON.parse( JSON.stringify( s ) );
        }, er=> alert( 'error' + er ) )
      }
    });
  }
  onClickSave(){
    if( ! this.validate ) return;
    this.reqService.saveItem( "list", this.itemForm, res=>{
      if( !this.isEdit ) this.socket.socketCreate( res );
      else this.socket.socketEdit( res );
      this.router.navigate([ 'home' ] );
    }, err=> alert( 'error'+err ))
  }
  get validate(){
    if( ! this.itemForm.title || !this.itemForm.description ) return false;
    else if( this.isEdit && !this.isChangedValue ) return false;
    return true;
  }

  get isChangedValue(){
    if( this.itemForm.title === this.itmForm2.title && this.itemForm.description === this.itmForm2.description  ) return false;
    return true;
  }
}

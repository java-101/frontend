import { Component } from "@angular/core";
import {Router} from "@angular/router";
@Component({
  selector: 'header',
  moduleId: module.id,
  templateUrl: 'header.template.html',
  styleUrls: [ 'header.template.scss' ]
})
export class HeaderTemplate{

  constructor( private route: Router){}

  nav( route: string ){
    this.route.navigate([ route ])
  }
}

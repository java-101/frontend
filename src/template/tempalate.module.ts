import { NgModule } from "@angular/core";
import {BodyTemplate} from "./body/body.template";
import {FooterTemplate} from "./footer/footer.template";
import {HeaderTemplate} from "./header/header.template";
import {ThemeTemplate} from "./theme/theme.template";
@NgModule({
  imports: [],
  declarations: [
    BodyTemplate,
    FooterTemplate,
    HeaderTemplate,
    ThemeTemplate
  ],
  exports: [ ThemeTemplate ],
  providers: []
})
export class TemplateModule{}

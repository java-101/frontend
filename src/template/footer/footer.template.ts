import { Component } from "@angular/core";
@Component({
  selector: 'footer',
  moduleId: module.id,
  templateUrl: 'footer.template.html',
  styleUrls: [ 'footer.template.scss' ]
})
export class FooterTemplate{}
